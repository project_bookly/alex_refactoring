package dhbw.fowler;

import java.util.*;

class Customer {
    private String name;
    private ArrayList<Rental> rentals = new ArrayList<Rental>();
    Customer(String name){
        this.name = name;
    }
    void addRental(Rental arg) {
        rentals.add(arg);
    }
    String getName(){
        return name;
    }
    String statement() {
        double totalAmount = 0;
        Iterator<Rental> enumRentals = rentals.iterator();
        StringBuilder res=new StringBuilder();
        res.append("Rental Record for ");
        res.append(this.getName());res.append("\n");
        res.append("\t");res.append("Title");res.append("\t");
        res.append("\t");res.append("Days");res.append("\t");
        res.append("Amount");res.append("\n");

        while (enumRentals.hasNext()) {
            Rental rental = enumRentals.next();
            //determine amounts for rental line
            double thisAmount = rental.movie.amountFor(rental);
            res.append("\t");res.append(rental.getMovie().getTitle());
            res.append("\t");res.append("\t");
            res.append(rental.getDaysRented());res.append("\t");
            res.append(rental.movie.amountFor(rental));res.append("\n");
            totalAmount += thisAmount;
        }
        //add footer lines
        res.append("Amount owed is ");
        res.append(totalAmount);res.append("\n");
        res.append("You earned ");
        res.append(getTotalFrequentRenterPoints());
        res.append(" frequent renter points");
        return res.toString();
    }

    private int getTotalFrequentRenterPoints(){
        int result = 0;
        for (Rental each : rentals) {
            result += getFrequentRenterPoints(each);
        }
        return result;
    }

    private int getFrequentRenterPoints(Rental rental){
        if(rental.getMovie().getPriceCode()==2){
            return 2;
        }
        return 1;
    }



}
    