
package dhbw.fowler;

public class Program
{

    
    public static void main(String args[]) 
    {
        String result;
        System.out.println("Welcome to the dhbw.fowler.Movie Store");
        Movie deadPool3 = new Movie("Dead Pool 3", 1);
        Movie mulan = new Movie("Mulan", 2);
        Rental rental1 = new Rental(deadPool3, 10);
        Rental rental2 = new Rental(mulan, 5);
        Customer customer = new Customer("joe");
        customer.addRental(rental1);   customer.addRental(rental2);
        System.out.println("Let's get the Statement");
        result = customer.statement();
        System.out.println(result);
    }
}


