package dhbw.fowler;

class Movie {
    private static final int REGULAR = 0;
    static final int NEW_RELEASE = 1;
    private static final int CHILDREN = 2;
    private String title;
    private int priceCode;
    Movie(String newtitle, int newpriceCode) {
        title = newtitle;
        priceCode = newpriceCode;
    }
    int getPriceCode() {
        return priceCode;
    }
    void setPriceCode(int arg) {
        priceCode = arg;
    }
    String getTitle(){ return title; }
    double amountFor(Rental rental) {
        double thisAmount = 0;
        switch (rental.getMovie().getPriceCode()) {
            case REGULAR:
                thisAmount += 2;
                if (rental.getDaysRented() > 2)
                    thisAmount += (rental.getDaysRented() - 2) * 1.5;
                break;
            case NEW_RELEASE:
                thisAmount += rental.getDaysRented() * 3;
                break;
            case CHILDREN:
                thisAmount += 1.5;
                if (rental.getDaysRented() > 3)
                    thisAmount += (rental.getDaysRented() - 3) * 1.5;
                break;
            default:
                throw new IllegalArgumentException("Incorrect dhbw.fowler.Price Code");

        }
        return thisAmount;
    }
}