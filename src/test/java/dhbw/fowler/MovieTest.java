package dhbw.fowler;


import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;


public class MovieTest {
    private String deadPoolTitle ="Dead Pool 3";

    @Test
    public void getPriceCode() {
        Movie deadPool3=new Movie(deadPoolTitle,1);
        assertThat(deadPool3.getPriceCode(),is(1));
    }

    @Test
    public void setPriceCode() {
        Movie deadPool3=new Movie(deadPoolTitle,0);
        deadPool3.setPriceCode(1);
        assertThat(deadPool3.getPriceCode(),is(1));
    }

    @Test
    public void getTitle() {
        Movie deadPool3=new Movie(deadPoolTitle,1);
        assertThat(deadPool3.getTitle(),is(deadPoolTitle));
    }

}
