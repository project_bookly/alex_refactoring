package dhbw.fowler;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class CustomerTest {
    private Movie mulan =new Movie("Mulan",2);
    private Movie deadPoolXYZ = new Movie("DeadPoolXYZ", 1);

    @Test
    public void customerTest1() {

        Rental rental1 = new Rental(deadPoolXYZ, 10);
        Rental rental2 = new Rental(mulan, 5);

        Customer customer = new Customer("max");
        customer.addRental(rental1);
        customer.addRental(rental2);

        String result = customer.statement();

        assertThat(result, is("Rental Record for max\n" +
                "\tTitle\t\tDays\tAmount\n" +
                "\tDeadPoolXYZ\t\t10\t30.0\n" +
                "\tMulan\t\t5\t4.5\n" +
                "Amount owed is 34.5\n" +
                "You earned 3 frequent renter points"));
    }

    @Test
    public void customerTest2() {
        Movie matrix = new Movie("matrix", 0);

        Rental rental = new Rental(matrix, 20);

        Customer customer = new Customer("luke");
        customer.addRental(rental);

        String result = customer.statement();

        assertThat(result, is("Rental Record for luke\n" +
                "\tTitle\t\tDays\tAmount\n" +
                "\tmatrix\t\t20\t29.0\n" +
                "Amount owed is 29.0\n" +
                "You earned 1 frequent renter points"));
    }



    @Test
    public void getName() {
        Customer customer = new Customer("max");
        assertThat(customer.getName(),is("max"));
    }

    @Test
    public void statement() {
        Movie matrix = new Movie("matrix", 0);
        Customer customer = new Customer("max");
        Rental rental = new Rental(matrix, 20);
        customer.addRental(rental);
        assertNotNull(customer.statement());
    }
}
